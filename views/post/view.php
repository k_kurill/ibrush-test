<?php
/**
 * @var $this yii\web\View
 * @var $model \app\models\Post
 */

use yii\helpers\Url;
use bigpaulie\social\share\Share;

$this->title = $model->title;

//OpenGraphs meta
$this->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
$this->registerMetaTag(['property' => 'og:type', 'content' => 'website']);
$this->registerMetaTag(['property' => 'og:image', 'content' => $model->imageUrl]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => '600']);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => '315']);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([],true)]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $model->description]);

//Twitter meta
$this->registerMetaTag(['property' => 'twitter:card', 'content' => $model->imageUrl]);
$this->registerMetaTag(['property' => 'twitter:image', 'content' => $model->imageUrl]);
$this->registerMetaTag(['property' => 'twitter:title', 'content' => $model->title]);
$this->registerMetaTag(['property' => 'twitter:description', 'content' => $model->description]);
?>
<h1><?=$model->title?></h1>

<div class="media">
    <div class="media-left">
        <img class="media-object" src="<?=$model->imageUrl?>" alt="<?=$model->title?>">
    </div>
    <div class="media-body">
        <h4 class="media-heading"><?=$model->title?></h4>
        <?=$model->description?>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <?= Share::widget([
            'include' => ['facebook', 'twitter', 'vk'],
            'template' => '<span> {button} </span>',
            'tag' => 'span',
        ]);?>
    </div>
</div>



