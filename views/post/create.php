<?php

/**
 * @var $this yii\web\View
 * @var $model \app\models\Post
 */

use app\widgets\Alert;
use yii\bootstrap\Html;
use kartik\file\FileInput;
use \yii\bootstrap\ActiveForm;

$this->title = 'Create post';
?>

<?= Alert::widget();?>
<?php $form = ActiveForm::begin();?>

<?= $form->errorSummary($model); ?>

<?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
    'pluginOptions' => [
        'allowedFileTypes' => ['image'],
        'maxImageHeight' => 800,
        'maxImageWidth' => 800,
        'allowedFileExtensions' => ['jpg'],
        'maxFileSize' => 500,
        'showUpload' => false,
        'browseLabel' => '',
        'removeLabel' => '',
        'mainClass' => 'input-group-lg'
    ]
]); ?>

<?= $form->field($model,'imageFontColor')->textInput(['type' => 'color']);?>

<?= $form->field($model, 'imageText');?>

<?= $form->field($model, 'imageFont')->dropDownList($model::getFontsArr());?>

<?= $form->field($model, 'title');?>

<?= $form->field($model, 'description')->textarea();?>

<?=Html::submitButton('Save', ['class'=>'btn btn-primary'])?>

<?php ActiveForm::end()?>
