<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $source
 * @property string $title
 * @property string $description
 * @property string $imageUrl
 * @property UploadedFile $imageFile
 * @property string $imageText
 * @property string $imageFont
 * @property string $imageFontColor
 */
class Post extends \yii\db\ActiveRecord
{

    public $imageFile;
    public $imageText;
    public $imageFont;
    public $imageFontColor;

    const UPLOAD_DIR = '@app/web/uploads';
    const FONT_DIR = '@app/fonts';

    const FONT_SIZE = 24;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imageFile'], 'file', 'maxSize' => 1024*1024*500],
            [['imageFile'], 'image', 'extensions' => 'jpg',
                'maxWidth' => 800,
                'maxHeight' => 800,
            ],
            [['imageFile', 'imageText', 'imageFont','imageFontColor'], 'required', 'on'=>'create'],
            [['description'], 'string'],
            [['source', 'title', 'imageText'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => 'Source',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }


    /**
     * @return string
     */
    public function getImageUrl()
    {
        return Url::home(true) . 'uploads/' .  $this->source;
    }

    /**
     * Allowed fonts
     * @return array
     */
    public static function getFontsArr()
    {
        return [
            'SpicyRice.ttf' => 'SpicyRice',
            'lazer84.ttf' => 'Lazer84',
            'chaos.ttf' => 'Chaos',
        ];
    }


    /**
     * upload, image with text, and save model
     * @throws Exception
     */
    public function upload()
    {
        if (!$this->validate()) {
            throw new Exception('Validation error!');
        }

        //making random subdirectory
        $dir = substr(md5(microtime()), mt_rand(0, 30), 2) . '/'
            . substr(md5(microtime()), mt_rand(0, 30), 2);
        $fileName = $dir . DIRECTORY_SEPARATOR . uniqid() . '.' . $this->imageFile->extension;
        FileHelper::createDirectory(Yii::getAlias(static::UPLOAD_DIR . DIRECTORY_SEPARATOR . $dir));

        //Put text on image
        $this->pasteText();

        $this->imageFile->saveAs(
            Yii::getAlias(static::UPLOAD_DIR)
            . DIRECTORY_SEPARATOR
            . $fileName
        );

        //saving filename
        $this->source = $fileName;
        $this->save(false);
    }

    /**
     * Put text on image in model
     */
    private function pasteText()
    {
        $image = imagecreatefromjpeg($this->imageFile->tempName);

        $font = Yii::getAlias(static::FONT_DIR . DIRECTORY_SEPARATOR . $this->imageFont);

        // Text positioning
        $box = imagettfbbox(static::FONT_SIZE, 0, $font, $this->imageText);
        $fontWidth = abs($box[4] - $box[0]);
        $fontHeight = abs($box[5] - $box[1]);
        $textWidth = $fontWidth;
        $xPos = (imagesx($image) - $textWidth) / 2;
        $yPos = (imagesy($image) + $fontHeight) / 2;

        list($r, $g, $b) = sscanf($this->imageFontColor, "#%02x%02x%02x");

        $color = imagecolorexact($image, $r,$g,$b);

        // Generate text
        imagefttext($image, static::FONT_SIZE, 0, $xPos, $yPos, $color, $font, $this->imageText);

        //Save image as just uploaded
        imagejpeg($image, $this->imageFile->tempName);

    }
}
