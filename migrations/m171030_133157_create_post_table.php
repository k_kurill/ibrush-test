<?php

use yii\db\Migration;

/**
 * Handles the creation of table `image`.
 */
class m171030_133157_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'source' => $this->string(),
            'title' => $this->string(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
