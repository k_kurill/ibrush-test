<?php
/**
 * Created by PhpStorm.
 * User: kurill
 * Date: 31.10.17
 * Time: 13:51
 */

namespace app\controllers;

use Yii;
use app\models\Post;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\UploadedFile;

class PostController extends Controller
{
    /**
     * Displays post create form.
     *
     * @return string
     */
    public function actionCreate()
    {
        $post = new Post(['scenario' => 'create']);
        if($post->load(Yii::$app->request->post())) {
            $transaction =Yii::$app->getDb()->beginTransaction();
            try{
                $post->imageFile = UploadedFile::getInstance($post, 'imageFile');
                $post->upload();
                $transaction->commit();
                return $this->redirect(['view', 'id' => $post->id]);
            } catch (Exception $exception) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('error', $exception->getMessage());
            }
        }
        return $this->render('create', ['model' => $post]);
    }

    public function actionView($id) {
        $post = Post::findOne($id);
        return $this->render('view', ['model' => $post]);
    }
}